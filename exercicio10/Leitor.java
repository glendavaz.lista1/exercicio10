import java.util.ArrayList;

public class Leitor
{
    private Peixe[] pexes;
    private ArrayList<Peixe> peixesList;    

    public Leitor()
    {
      pexes = new Peixe[10];
      peixesList = new ArrayList<>();
    }
    
    public ArrayList<Peixe> getPeixesList() {
        return peixesList;
    }
    
    public void addPeixe(Peixe peixe) {
        this.peixesList.add(peixe);
    }
    
  
    public double calculaMulta() {
        double multa = 0;
        for(int i = 0; i < peixesList.size(); i++) {
            Peixe peixe = peixesList.get(i);
            
            if(peixe.getPeso() > 50) {
                multa = multa + ((peixe.getPeso() - 50) * 4.0);
            }
        }
        
        return multa;
    }
}
